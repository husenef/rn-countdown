import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
  },
  clearfix: {
    flex: 1,
  },
  containerTime: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    // backgroundColor: 'red',
  },
  textCenter: {
    alignSelf: 'center',
  },
  fontBig: {
    fontSize: 80,
    fontWeight: 'bold',
  },
  fontSmall: {
    fontSize: 40,
    fontWeight: 'bold',
  },
});
export default styles;
