import React, {Component} from 'react';
import {Text, View, ToastAndroid} from 'react-native';

import Styles from './style';

class Countdown extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hours: 0,
      minutes: 0,
      seconds: 0,
      isStart: false,
      time: 0,
      intervalId: 0,
    };

    this.initializeState = {};
  }

  componentDidMount() {
    this.initializeState = this.state;
  }

  componentDidUpdate(prevProps) {
    const {isStart} = this.state;
    if (this.props.start && !isStart) {
      this.startCountDown();
    }

    if (!this.props.start && this.props.stop && isStart) {
      this.stop();
      ToastAndroid.show('Time canceled', ToastAndroid.LONG);
    }
  }

  startCountDown = () => {
    const {endtime, isStart} = this.props;
    const today = new Date();
    const enddata = today.getTime() + endtime - 1000;
    const firstDiff = this.parserMilisecond(enddata - today.getTime());

    this.setState({
      isStart: true,
      time: enddata,
      hours: firstDiff.h,
      minutes: firstDiff.m,
      seconds: firstDiff.s,
    });
    if (!isStart) {
      const id = setInterval(() => {
        const {intervalId} = this.state;
        const current = new Date();

        this.updateTime();
        if (current.getTime() > enddata) {
          clearInterval(intervalId);
          this.reset();
          ToastAndroid.show('Time is Done', ToastAndroid.LONG);
        }
      }, 1000);

      this.setState({intervalId: id});
    }
  };

  updateTime = () => {
    const {hours, minutes, seconds} = this.state;
    let numHours = parseFloat(hours) || 0;
    let numMinutes = parseFloat(minutes) || 0;
    let numSeconds = parseFloat(seconds) || 0;

    if (numSeconds === 0) {
      if (numMinutes > 0) {
        numMinutes = numMinutes - 1;
      }
      numSeconds = 60 - 1;
    } else {
      numSeconds = numSeconds - 1;
    }

    if (numMinutes === 0) {
      if (numHours > 0) {
        numHours = numHours - 1;
        numMinutes = 60 - 1;
      }
    }
    this.setState({seconds: numSeconds, minutes: numMinutes, hours: numHours});
  };

  parserMilisecond = s => {
    const ms = s % 1000;
    s = (s - ms) / 1000;
    const secs = s % 60;
    s = (s - secs) / 60;
    const mins = s % 60;
    const hrs = (s - mins) / 60;
    return {
      h: hrs,
      m: mins,
      s: secs,
    };
  };

  setText = (key, val) => {
    let {time} = this.state;
    val = val || 0;
    if (key === 'seconds') {
      if (val >= 60) {
        ToastAndroid.show('Seconds no more than 60', ToastAndroid.LONG);
        return false;
      }
      time = time + val * 1000;
    }
    if (key === 'minutes') {
      if (val >= 60) {
        ToastAndroid.show('Minutes no more than 60', ToastAndroid.LONG);
        return false;
      }
      time = time + val * 60 * 1000;
    }
    if (key === 'hours') {
      if (val >= 99) {
        ToastAndroid.show('Hours no more than 99', ToastAndroid.LONG);
        return false;
      }
      time = time + val * 24 * 60 * 1000;
    }

    this.setState({[key]: val, time: time});
  };

  parseTime = time => {
    return time < 10 ? `0${time}` : time;
  };

  stop = () => {
    clearInterval(this.state.intervalId);
    this.setState({
      isStart: false,
      intervalId: 0,
      time: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
    });
  };
  reset = () => {
    clearInterval(this.state.intervalId);
    this.props.callback(false);
    this.setState({
      isStart: false,
      intervalId: 0,
      time: 0,
      hours: 0,
      minutes: 0,
      seconds: 0,
    });
  };

  render() {
    const {hours, minutes, seconds} = this.state;
    return (
      <View style={Styles.container}>
        <View style={Styles.containerTime}>
          <Text style={[Styles.fontBig, Styles.textCenter]}>
            {this.parseTime(hours)}
          </Text>
          <Text style={[Styles.fontSmall, Styles.textCenter]}>:</Text>
          <Text style={[Styles.fontBig, Styles.textCenter]}>
            {this.parseTime(minutes)}
          </Text>
          <Text style={[Styles.fontSmall, Styles.textCenter]}>:</Text>
          <Text style={[Styles.fontBig, Styles.textCenter]}>
            {this.parseTime(seconds)}
          </Text>
        </View>
      </View>
    );
  }
}

export default Countdown;
