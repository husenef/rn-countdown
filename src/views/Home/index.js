import React, {Component} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';

import {Countdown} from '../../components';

import Styles from './style';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isStart: false,
      time: 0,
      isEnd: false,
      isStop: false,
      hours: '',
      minutes: '',
      seconds: '',
    };
    this.initializeState = {};
  }

  componentDidMount() {
    this.initializeState = this.state;
  }

  setText = (key, val) => {
    if ((key === 'minutes' || key === 'seconds') && val > 60) {
      ToastAndroid.show(`${key} no more than 60`, ToastAndroid.LONG);
      return false;
    }
    if (key === 'hours' && val > 99) {
      ToastAndroid.show('Hours no more than 99', ToastAndroid.LONG);
      return false;
    }
    this.setState({[key]: val});
  };

  callTime = () => {
    const {hours, minutes, seconds, time} = this.state;
    let newTime = time;
    const timeH = hours * 60 * 60 * 1000;
    const timeM = minutes * 60 * 1000;
    const timeS = seconds * 1000;
    newTime = timeH + timeM + timeS;
    this.setState({time: newTime, isStart: true});
  };

  stop = () => {
    this.setState({...this.initializeState, isStop: true});
  };
  reset = () => {
    const initializeState = this.initializeState;
    this.setState({...initializeState, isEnd: false, isStart: false});
  };

  parseText = val => {
    return val < 10 ? `0${val}` : val;
  };

  render() {
    const {isStart, isEnd, time, isStop, hours, minutes, seconds} = this.state;

    return (
      <View style={Styles.container}>
        <View style={Styles.containerTop}>
          <Countdown
            start={isStart}
            stop={isStop}
            endtime={time}
            callback={start => this.setState({isStart: start, isEnd: true})}
          />
        </View>

        <View style={Styles.containerBtm}>
          {!isStart && !isEnd && (
            <View style={Styles.containerForm}>
              <TextInput
                placeholder={'00'}
                style={Styles.TextInputStyle}
                onChangeText={text => this.setText('hours', text)}
                keyboardType={'numeric'}
                dataDetectorTypes={'phoneNumber'}
                value={`${hours}`}
              />
              <TextInput
                placeholder={'00'}
                style={Styles.TextInputStyle}
                onChangeText={text => this.setText('minutes', text)}
                keyboardType={'numeric'}
                dataDetectorTypes={'phoneNumber'}
                value={`${minutes}`}
              />
              <TextInput
                placeholder={'00'}
                style={Styles.TextInputStyle}
                onChangeText={text => this.setText('seconds', text)}
                keyboardType={'numeric'}
                dataDetectorTypes={'phoneNumber'}
                value={`${seconds}`}
              />
              <TouchableOpacity
                onPress={() => this.callTime()}
                style={Styles.btn}>
                <Text>Start</Text>
              </TouchableOpacity>
            </View>
          )}
          {isStart && (
            <View style={Styles.clearfix}>
              <Text style={Styles.fontSmall}>Countdown Running...</Text>
              <TouchableOpacity style={Styles.btn} onPress={() => this.stop()}>
                <Text style={Styles.textCenter}>Stop</Text>
              </TouchableOpacity>
            </View>
          )}
          {!isStart && isEnd && (
            <View style={Styles.clearfix}>
              <Text style={Styles.fontSmall}>Time is End</Text>
              <TouchableOpacity style={Styles.btn} onPress={() => this.reset()}>
                <Text style={Styles.textCenter}>Reset</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    );
  }
}

export default Home;
