import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
  },
  clearfix: {
    flex: 1,
  },
  containerTop: {
    flexDirection: 'row',
    flex: 3,
  },
  containerBtm: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerTime: {
    flex: 1,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    // backgroundColor: 'red',
  },
  containerForm: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  TextInputStyle: {
    textAlign: 'center',
    height: 40,
    // borderRadius: 10,
    borderWidth: 1,
    borderColor: '#cdcdcd',
    marginBottom: 10,
    marginHorizontal: 5,
    minWidth: 50,
  },
  btn: {
    textAlign: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
    height: 40,
    paddingHorizontal: 5,
    borderRadius: 4,
    borderColor: '#cdcdcd',
    borderWidth: 2,
  },
  textCenter: {
    textAlign: 'center',
  },
});
export default styles;
